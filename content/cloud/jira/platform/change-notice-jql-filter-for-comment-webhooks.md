---
title: "Change notice - JQL filters applied to comment webhooks in Jira Cloud"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2017-12-14"
---

# Change notice - JQL filters applied to comment webhooks in Jira Cloud

From 19 December 2017 JQL filters defined in Jira Cloud webhooks will be applied for `comment` events.

The following webhooks will respect the JQL filter defined, and will be fired only for issue comments that meet the JQL criteria:

- `comment_created`
- `comment_deleted`
- `comment_updated`

This change fixes the following bug [JRACLOUD-68235: JQL filter for webhooks is not applied to "comment" events](https://jira.atlassian.com/browse/JRACLOUD-68235).

This change will be rolled out gradually, starting from 19 December 2017 and it may take up to 3 weeks until it reaches all Jira Cloud instances.
Note, that after this change is fully rolled out, `comment` objects will be removed from `jira:issue_*` webhooks, as announced in the [deprecation notice](/cloud/jira/platform/deprecation-notice-removal-of-comment-object-data-in-jira-issue-webhooks/).

## What will happen if I do nothing?

For every webhook that is registered with a JQL filter and `comment` events, your app will start receiving fewer webhooks with `comment` events, because they will be restricted to only the ones that respect the JQL filter.

If your app expected that the `comment` webhooks were only fired for issue comments that match the JQL - great news, as you don't need to do anything.

And if your app contains some logic that filters out the irrelevant `comment` events, you may consider removing it, because the client will now receive filtered webhooks.

## Replacement

If your app should receive all `comment` related events, register a separate webhook for desired `comment` events without setting a JQL filter for them.

Learn more about Jira Cloud [webhooks](/cloud/jira/platform/webhooks/).

Report feature requests and bugs for Jira Cloud and webhooks in the [ACJIRA project](https://ecosystem.atlassian.net/projects/ACJIRA) on ecosystem.atlassian.net.
