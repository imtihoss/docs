---
title: "Extension points for the end-user UI"
platform: cloud
product: jswcloud
category: reference
subcategory: modules
date: "2017-08-24"
---

{{< reuse-page path="docs/content/cloud/jira/platform/extension-points-for-the-end-user-ui.md">}}