# Atlassian developer documentation

This repository contains the content for developer.atlassian.com (DAC). It is
made of markdown and data files that can be processed by the static generator
Hugo to generate HTML pages.

## Contributors

Pull requests, issues, and comments welcome. For pull requests:

* follow the existing style
* separate unrelated changes into multiple pull requests

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a 
[Contributor License Agreement](https://atlassian.wufoo.com/forms/contributor-license-agreement/),
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code, documentation, or translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions, we ask that you please follow the 
link below to digitally sign the CLA:

* [Contributor License Agreement](https://atlassian.wufoo.com/forms/contributor-license-agreement/)

## Internal contributors

You can preview your changes as you make them by following
[this guide](https://developer.atlassian.com/dac/viewing-docs-locally/).
Note that this resource is restricted to internal Atlassians.
You will need to log in with your Atlassian email address. 

## License

Copyright (c) 2016-2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

