---
title: "Connect cookbook"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
date: "2017-10-19"
---

# Connect cookbook

This cookbook is an extension of the [JavaScript API Cookbook](/cloud/confluence//about-the-javascript-api/#cookbook),
which provides snippets of code that can be used in the clients browser to get information from the product.

## Getting Confluence spaces

This retrieves a list of spaces from Confluence, and may require paging through the results to see all 
spaces from your instance.

``` javascript
AP.require('request', function(request) {
  request({
    url: '/rest/api/space',
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }  
  });
});
```

## Getting specific spaces from Confluence

This snippet lets you request a specific Confluence space by space key. In this example, we use 
`ds`. This recipe also provides some high-level information about the space. If you're looking for 
more information about a space, you can find out about the content in the space in the next example, 
using `/rest/api/space/{space.key}/content`.


``` javascript
AP.require('request', function(request) {
  request({
    url: '/rest/api/space/ds',
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }   
  });
});
```

## Getting pages in a space

This recipe returns a collection for a given space identifier (like `ds` in the example below), 
containing objects like a blog post and one of the pages in the space. This returns a collection 
for the given space identifier (e.g. `ds`) that contains a object of blog posts and one of
pages in the space. You can also directly access pages (`/rest/api/space/ds/content/page`) or 
blog posts (`/rest/api/space/ds/content/blogpost`) if you want to page though the contents. 

``` javascript
var space
AP.require('request', function(request) {
  request({
    url: '/rest/api/space/ds/content',
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }    
  });
});
```