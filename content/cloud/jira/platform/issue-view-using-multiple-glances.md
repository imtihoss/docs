---
title: Use multiple glances in the issue view
platform: cloud
product: jiracloud
category: devguide
subcategory: design
date: "2018-04-24"
---

# Use multiple glances in the issue view

{{% note title="New Jira issue view" %}}

<br>
These design guidelines relate to the [new Jira issue view](https://confluence.atlassian.com/display/JIRACORECLOUD/The+new+Jira+issue+view), which is currently only available to some users in Jira Software. The new modules and extension patterns on this page only relate to Jira Software right now, but they'll be relevant for Jira Service Desk in future. [Learn more about the Alpha preview](/blog/2017/12/alpha-program-for-the-new-jira-issue-view/).

{{% /note %}}

As an app developer, you can define multiple glance modules in your descriptor, but Jira will only display the first glance module returned (after any [conditions](https://developer.atlassian.com/cloud/jira/platform/conditions/) have been evaluated). We recommend you avoid selecting conditions that result in more than one glance appearing on an issue.

<br>

----

<br>

## Using multiple glances to represent different entities

If your app has multiple entities you want to represent using glances, you can make use of [conditions](/cloud/jira/platform/conditions/). For example, if you're building a Git integration, you might want to show related commits, branches, and pull requests for an issue.

In this example, you could define a glance module in your descriptor for each of these:

- Commits
- Branches
- Pull requests

You could then make use of [conditions](/cloud/jira/platform/conditions/) to validate against [properties that you store against the issue](https://developer.atlassian.com/cloud/jira/platform/storing-data-without-a-database/) to choose the glance to be displayed.

For each glance you could use a [predefined condition](/cloud/jira/platform/conditions/), like entity_property_equal_to. You could then define the condition to decide which glance to display.

- Commits: `'git_integration_display_glance === commits'`
- Branches: `'git_integration_display_glance === branches'`
- Pull requests: `'git_integration_display_glance === pull-requests'`

<br>

----

<br>

## Using multiple glances for empty states

Your app may want to have an experience where you want users to take an action before you display a glance. You can define multiple glances to create this experience.

For example, through using [conditions](/cloud/jira/platform/conditions/) and [app properties](https://developer.atlassian.com/cloud/jira/platform/storing-data-without-a-database/), you could define a glance that prompts users to "Link a conversation".

<p style="text-align: center;"><img src="/cloud/jira/platform/images/Call-to-action-glance.png" alt="Glance with call to action" width="400"></p>

When a user clicks your glance, you could use the modal dialog target to design a view where the user links a conversation to the issue through your app.

Once a conversation is linked, you could use [conditions](/cloud/jira/platform/conditions/) and [app properties](/cloud/jira/platform/storing-data-without-a-database/) to change your glance to communicate that a conversation has been linked.

<p style="text-align: center;"><img src="/cloud/jira/platform/images/Call-to-action-glance-linked.png" alt="Glance with linked conversation" width="400"></p>

<br>
