---
title: "Latest updates for Jira Service Desk"
platform: cloud
product: jsdcloud
category: devguide
subcategory: index
date: "2017-09-11"
---

# Latest updates

We deploy updates to Jira Service Desk Cloud frequently. As a Jira developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

## Recent changes

#### Request create property panel

We have added support for defining your own [request create property panels](request-create-property-panel) which
are displayed on the request creation screen in the customer portal and enable apps to save arbitrary data during
request creation as Jira issue properties.

#### Issue fields support

Single and multi select [issue fields](modules/issue-field) supplied by Connect apps are now supported in JSD customer portal.

#### Non-experimental APIs

We have removed the experimental flag from the following APIs:

* `/servicedeskapi/customer`
* `/servicedeskapi/servicedesk/{serviceDeskId}/customer` (POST only)
* `/servicedeskapi/request/{issueIdOrKey}/approval`
* `/servicedeskapi/request/{issueIdOrKey}/approval/{approvalId}`

#### Updated REST APIs

We have made the following changes to the REST APIs:

* Added the `searchQuery` parameter to `/servicedeskapi/requesttype`
* Added the `approvalStatus` parameter and the `requestOwnership=APPROVER` value to `/servicedeskapi/request`
* Added `/servicedeskapi/request/{issueIdOrKey}/notification`

For up-to-date and complete docs, see the [JSD REST API documentation](/cloud/jira/service-desk/rest/).

#### Platform
Also see the [latest updates for the Jira Platform](../platform).

## Atlassian Developer blog

Major changes that affect Jira Cloud developers are announced in the **Atlassian Developer blog**, like new Jira modules or the deprecation of API end points.
You'll also find handy tips and articles related to Jira development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/)
 ([Jira-related posts](https://developer.atlassian.com/blog/categories/jira/))

## Release notes

Changes to Jira Service Desk Cloud are announced on the Jira Service Desk blog.

Changes that affect all Jira Cloud products are announced in the *What's New blog* for Atlassian Cloud. This includes new features, bug fixes, and other changes. For example, the introduction of a new Jira quick search or a change in project navigation.

Check them out and subscribe here:  
[Jira Service Desk blog](https://confluence.atlassian.com/pages/viewrecentblogposts.action?key=SERVICEDESKCLOUD)  
[What's new blog](https://confluence.atlassian.com/display/Cloud/What%27s+New) (Note, this blog also includes changes to other Cloud applications)
