---
title: Scopes
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
aliases:
- /cloud/jira/software/scopes.html
- /cloud/jira/software/scopes.md
date: "2017-08-25"
---
{{< include path="docs/content/cloud/connect/reference/jira-scopes.snippet.md">}}