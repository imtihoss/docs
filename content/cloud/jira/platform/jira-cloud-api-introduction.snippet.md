## Atlassian Connect

Atlassian Connect is the foundation for many types of Jira Cloud apps. It handles 
discovery, installation, authentication, and seamless integration into the Jira UI. An 
Atlassian Connect app could be an integration with an existing service, a new feature 
for Jira, or even a new product that runs within Jira. 

With Atlassian Connect, apps add content or features—pages, panels, reports, JQL 
functions, gadgets—in certain defined places in Jira Cloud's UI via modules. Atlassian 
Connect also provides a way to make secure requests to the Jira Cloud REST APIs and 
register for secure webhooks from Jira Cloud.

You can write an app with any programming language and web framework, and deploy it in 
almost any way you can imagine. From massive SaaS services to static apps served 
directly from a code repo, Atlassian Connect is designed to let you connect anything to 
Atlassian products.

For example, the <a href="https://marketplace.atlassian.com/plugins/whoslooking-connect/cloud/overview" class="external-link">&quot;Who's looking&quot; app</a> (*<a href="https://bitbucket.org/atlassian/whoslooking-connect" class="external-link">source</a>*) 
is a Java application that runs on the Play framework and is hosted on Heroku. It uses 
the Jira REST API to retrieve the details of users viewing an issue, persists it 
locally (checking if users are still watching via an XHR heartbeat), and displays it in 
a web panel module on the Jira issue view screen.

### Overview

Atlassian Connect apps are essentially web applications that operate remotely over 
HTTP. To an end user, an app appears as a fully integrated part of Jira Cloud. Once 
your app is installed in Jira Cloud, features are delivered from the Jira Cloud UI. 
This deep level of integration is part of what makes Atlassian Connect apps so powerful.

![Jira cloud integration graphic](/cloud/jira/platform/images/jira-connect-overview-adg3.png)

#### The app descriptor

The basic building block of an Atlassian Connect app is the app descriptor. The app 
descriptor is a JSON which is served by the app, which does the following:

-   provides basic information about the app (name, vendor, where it's hosted, etc),
-   handles the installation lifecycle,
-   handles authorization (scopes) and authentication, and
-   describes the modules that your app uses to the application (read more about [modules](#extend-jira-cloud-with-modules) below).

For more information on how the app descriptor works, see [App descriptor].

## Extend Jira Cloud with modules

Modules are the most important component of your Jira app or integration. These are the 
**integration points** that your app uses to provide rich interactions with Jira. There 
are two types of modules: basic iframes that allow you to display content in different 
places in Jira, and more advanced modules that let you provide advanced Jira-specific 
functionality. Jira Service Desk and Jira Software also have their own 
application-specific modules (UI related only).

If you plan to integrate with the Jira Cloud user interface, check out [Extending the Jira Cloud UI], or jump straight into the [modules API reference].

## REST APIs

The Jira REST APIs are used to interact with Jira remotely. For most communication with 
Jira Cloud, you will use the Jira REST APIs and webhooks. 

#### [Jira Cloud REST API] 

The Jira platform REST API covers most of the primary operations with basic Jira 
objects, like issues, projects, users, dashboard, and more. This REST API is common to 
all Jira products.

#### [Jira Software Cloud REST API]

The Jira Software REST API covers advanced features specific to Jira Software, like boards, sprints, epics, and more.

#### [Jira Service Desk Cloud REST API]

The Jira Service Desk REST API covers advanced features specific to Jira Service Desk, like customer requests, queues, and SLAs.

## Webhooks

Webhooks are outgoing messages from Jira that allow your app or integration to react to events, like someone transitioning an issue or closing a sprint. Like the REST APIs, there is a set of platform-level webhooks and additional advanced webhooks for Jira Software and Jira Service Desk.

To learn more about webhooks, read [webhooks].

## Entity properties

Entity properties are key-value stores attached to Jira objects, which can be created, 
updated, and deleted via the Jira REST APIs. This is a powerful system for storing data 
on the Jira host; it easily support imports, exports, and migrations between instances 
because the data is stored locally with the Jira Cloud tenant. Here's how entity 
properties can help you with your Jira Cloud integration:

-   Simplify your app or integration by reducing how much data you store on your own server.
-   Improve performance by evaluating `entity_property_equal_to` conditions in-process.
-   Implement advanced features like [JQL integration using search extractions].

The following entities can have entity properties added to them:

|              |                 |
|--------------|-----------------|
| **Jira (all products)** | <ul><li>issues</li><li>comments</li><li>projects</li><li>users</li><li>issue types</li><li>dashboard items</li></ul> |
| **Jira Software** | <ul><li>sprints</li><li>boards</li></ul> |
| **Atlassian Connect apps** | Atlassian Connect provides <strong>app properties</strong> that are scoped specifically to your app and only accessible by you. This is a great place to store tenant-specific configuration data.<br>Read [Storing data without a database] for more details.  |

To learn more about Jira entity properties, read [entity properties].

## UI tools

The Atlassian Connect JavaScript API, conditions, and context parameters make it easier for your app to interact with the Jira UI.

### Atlassian Connect JavaScript API

The Atlassian Connect framework provides a JavaScript API to allow your app iframe to 
interact with the content around it on the page in Jira. The Atlassian Connect 
JavaScript API is provided by Jira; simply include the `all.js` file in the scripts in 
each of your pages. The JavaScript API includes methods for:

-   messaging between multiple iframes
-   automatic resizing for your content
-   advanced Jira UI actions like opening date pickers and the create issue dialog
-   making XHR requests to Jira REST resources without requiring CORS
-   and much more!

To learn more, read the [JavaScript API documentation].

### Conditions

More often than not, you won't want to load your modules on every page, for every user, 
every time. You can use **conditions** in your app descriptor to determine whether Jira 
should load your app's UI modules. For example, you may only want to load a certain 
panel if the current user is an administrator, or if a certain property on an issue has 
been configured. 

To set up conditions, define which ones are applicable when you declare each module in 
your app descriptor and Jira will evaluate them in-process when the page is loaded. For 
example:

``` javascript
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in"
                    }
                ]
            }
        ]
    }
}
```

To learn more, read [conditions].

### Context parameters

Frequently, the content you show in a panel will often vary depending on the context 
around it (like the current issue, board, user, or project). When Jira makes a request 
to your app to load an iframe, it can send additional context parameters that can tell 
your app how to respond to the request and which content to load. There are some 
standard context parameters (like user language and timezone) as well as Jira specific 
parameters. 

To learn more, read [context parameters].

### Developing with Atlassian Connect

You can use Atlassian Connect to build a range of integrations with different Atlassian Cloud applications. However, regardless of what you are building, you'll need to understand the following key development processes:

#### Security

Security is critical in a distributed component model such as Atlassian Connect. 
Atlassian Connect relies on HTTPS and JWT authentication to secure communication 
between your app, the Atlassian product, and the user.

Your app's actions are constrained by well-defined permissions, and it can only make 
API requests based on the scopes in its descriptor. These permissions are granted by 
Atlassian application administrators when they install your app. Examples of 
permissions include reading content, creating pages, creating issues, and more. These 
permissions help ensure the security and stability of cloud instances.

Read our [security overview] for more details.

#### Atlassian design guidelines

Since Atlassian Connect apps can insert content directly into the Atlassian host application, it is critical that apps are visually compatible with the Atlassian application's design. Our designers and developers have created a number of resources to help you with this:<ul><li><a href="https://design.atlassian.com/product/" class="external-link">Atlassian Design Guidelines</a> — Our design guidelines define core interactions with the Atlassian applications.</li><li><a href="https://atlaskit.atlassian.com" class="external-link">AtlasKit</a> — AtlasKit is a library of reusable front-end UI components.</li></ul>

#### The Atlassian Marketplace

List your app on the [Atlassian marketplace] so other Jira Cloud users can install it. 
You can list your app privately on the site if you don't intend to sell or distribute 
your code, but all apps need to be listed in order to be installable. Private listings 
are supported with secret tokens that you can generate yourself.<br>Read our [selling 
on marketplace page] for more details.

Atlassian Marketplace also lets you sell your app commercially. Read our [licensing guide] for more details.

## Get started

Nice work! You made it through the whole introduction. Time to get hands-on with Jira Cloud development. Read our [Getting started guide] to learn how to set up a development environment and build a Jira Cloud app.

  [Architecture overview]: /cloud/jira/platform/architecture-overview
  [App descriptor]: /cloud/jira/platform/app-descriptor
  [security overview]: /cloud/jira/platform/security-overview
  [Atlassian marketplace]: https://marketplace.atlassian.com
  [selling on marketplace page]: /platform/marketplace/selling-on-marketplace/
  [licensing guide]: /platform/marketplace/cloud-app-licensing/
  [About Jira modules]: /cloud/jira/platform/about-jira-modules 
  [conditions]: /cloud/jira/platform/conditions
  [context parameters]: /cloud/jira/platform/context-parameters
  [JQL integration using search extractions]: /cloud/jira/platform/jira-entity-properties
  [storing data without a database]: /cloud/jira/platform/storing-data-without-a-database/
  [entity properties]: /cloud/jira/platform/jira-entity-properties
  [Jira Cloud REST API]: /cloud/jira/platform/rest/
  [Jira Software Cloud REST API]: https://docs.atlassian.com/jira-software/REST/cloud/
  [Jira Service Desk Cloud REST API]: /cloud/jira/service-desk/rest/
  [JavaScript API documentation]: /cloud/jira/platform/about-the-javascript-api
  [webhooks]: /cloud/jira/platform/webhooks
  [Getting started guide]: /cloud/jira/platform/getting-started
  [Extending the Jira Cloud UI]: /cloud/jira/platform/extending-the-user-interface
  [modules API reference]: /cloud/jira/platform/frameworks-and-tools