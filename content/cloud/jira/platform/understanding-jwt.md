---
title: "Understanding JWT for apps"
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
- /cloud/jira/platform/understanding-jwt.html
- /cloud/jira/platform/understanding-jwt.md
date: "2017-08-25"
---

{{< include path="docs/content/cloud/connect/concepts/understanding-jwt.snippet.md" >}}
