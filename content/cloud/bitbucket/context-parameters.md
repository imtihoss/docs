---
title: "Context parameters"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: blocks 
date: "2016-09-16"
---

# Context parameters 

Context parameters are placeholders you can put into a module's URL to have Bitbucket send contextual information with each request to that module. They follow the same pattern as if they were normal API objects.

In fact you can deduce all the parameters from the API JSON object. Let's start by looking at [an example repository object](https://api.bitbucket.org/2.0/repositories/teamsinspace/documentation-tests):

``` json
{
    "scm": "git",
    "website": "",
    "has_wiki": true,
    "name": "documentation-tests",
    "links": {...},
    "fork_policy": "allow_forks",
    "uuid": "{b4434b4d-6a0e-4f57-8d75-e02a824abeb0}",
    "project": {...},
        "name": "Master station"
    },
    "language": "",
    "created_on": "2014-07-24T21:48:26.648365+00:00",
    "full_name": "teamsinspace/documentation-tests",
    "has_issues": true,
    "owner": {
        "username": "teamsinspace",
        "display_name": "Teams In Space ",
        "type": "team",
        "uuid": "{61fc5cf6-d054-47d2-b4a9-061ccf858379}",
        "links": {...}
    }, 
    ...
}
```
In that object you can see:

``` json
"full_name": "teamsinspace/documentation-tests"
```

We need to include the repository parameter first since that's the object we're looking at. To call the 
`full_name` context parameter you would use the format: 

``` json
https://example.com?repoPath={repository.full_name}
```
Which might look like this in the descriptor: 

``` json
        "repoPage": [
            {
                "url": "https://example.com?repoPath={repository.full_name}",
                "name": {
                    "value": "Example Page"
                },
                "location": "org.bitbucket.repository.navigation",
                "key": "example-repo-page",
                "params": {
                    "auiIcon": "aui-iconfont-doc"
                }
            }
        ],
```

### Let's dig in a little deeper

Let's say you want the `username` of a repository's owner displayed in a module.

The context parameter might look like this:  

``` bash
https://example.com?repoOwner={repository.owner.username}
```
The parameters it references are `repository.owner.username`. 

When you look at the response object from a `repository` API call you can see the `username` nested within the `owner` parameter: 

``` json
{...
    "uuid": "{b4434b4d-6a0e-4f57-8d75-e02a824abeb0}",
    "project": {...
    },
    "language": "",
    "created_on": "2014-07-24T21:48:26.648365+00:00",
    "full_name": "teamsinspace/documentation-tests",
    "has_issues": true,
    "owner": {
        "username": "teamsinspace",
        "display_name": "Teams In Space ",
        "type": "team",
        "uuid": "{61fc5cf6-d054-47d2-b4a9-061ccf858379}",
        "links": {...
            }
        }
    },
    "updated_on": "2016-07-29T18:45:36.317590+00:00",
    "size": 1172663,
    "type": "repository",
    "is_private": false,
    "description": ""
```
You format your parameter as {repository.owner.username} because the repository has the owner and the owner
has the username. 

Your parameter might look like this in the descriptor: 

``` json
        "webItem": [
            {
                "url": "https://example.com?repoOwner={repository.owner.username}",
                "name": {
                    "value": "Example Web Item"
                },
                "location": "org.bitbucket.repository.navigation",
                "key": "example-web-item",
                "params": {
                    "auiIcon": "aui-iconfont-link"
                }
            }
        ],
```
You can expand this base concept to create more complex calls using context parameters like so: 

``` bash
https://myapp.example.com/pages/repos/{pullrequest.to_repository.full_name}/pulls/{pullrequestid}?currentUser={user.uuid}
```
Even more complex: 

``` bash
https://myapp.example.com/pages/repos/{issue.repository.uuid}/issuepage/{issue.id}/comments?reporter={issue.reporter.username}&issueLink={issue.links.html.href}
```
### Context parameters in path segments 

Variables are replaced by their runtime value. In some cases your parameter's value might be an empty string.
This can be problematic when you use it as part of your path. For instance, if a repository object does not
have a description, `/repos/{repository.description}/foo/` turns into `/repos//foo/`, which many webservers and
frameworks might normalize as if there was only one slash (`/repos/foo/`), which could effectively make the
variable disappear. 

To avoid this potential ambiguity you could move the parameters into the query string.

``` bash
/repos/foo?currentUser={user.username}
```

## Location and contexts

The location of the module determines which context parameters are available for use. The general idea is that if you can do it on that page those contexts apply. 

For example a `webItem` on the Pull Request page could have these parameters: 

* pullrequest
* repository
* user
* target_user
 
### User and target_user

The `user` is the currently authenticated user. This is the person actually using Bitbucket and your app.

{{% note %}}The `user` object will be read as an empty field when called as part of an anonymous page view.{{% /note %}}

The `target_user` is the principal owner of the thing (repository, snippet, file, other...) the `user` is viewing or interacting with.

### Context parameters based on module 

The following context objects are available for each of these modules. 

<table class='aui'>
    <thead>
        <tr>
            <th>Module</th>
            <th>Available context objects</th>
        </tr>
    </thead>
    <tr>
        <td><code>adminPage</code></td>
        <td><ul>
            <li><code>target_user</code></li>
            <li><code>user</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td><code>configurePage</code></td>
        <td><ul>
            <li><code>target_user</code></li>
            <li><code>user</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td><code>fileViews</code></td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
                <li><code>file</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td><code>profileTabs</code></td>
        <td><ul><li><code>target_user</code></li>
                <li><code>user</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td><code>repoPage</code></td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>repository</code></li>
                <li><code>user</code></li>
            </ul>
        </td>
    </tr>
</table>

### Context parameters based on location    

The following context objects are available for these locations. 

<table class='aui'>
    <thead>
        <tr>
            <th>Location</th>
            <th>Location code</th>
            <th>Available context objects</th>
        </tr>
    </thead>
    <tr>
        <td>Repository admin page</td>
        <td>org.bitbucket.repository.admin</td>
        <td><ul>
            <li><code>target_user</code></li>
            <li><code>user</code></li>
            <li><code>repository</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Account admin page</td>
        <td>org.bitbucket.account.admin</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Repository overview information panel</td>
        <td>org.bitbucket.repository.overview.informationPanel</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Repository actions</td>
        <td>org.bitbucket.repository.actions</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Pull request summary information area</td>
        <td>org.bitbucket.pullrequest.summary.info</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
                <li><code>pullrequest</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Pull request summary actions area</td>
        <td>org.bitbucket.pullrequest.summary.actions</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
                <li><code>pullrequest</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Branches summary information area</td>
        <td>org.bitbucket.branch.summary.info</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
                <li><code>branch</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Branches summary actions area</td>
        <td>org.bitbucket.branch.summary.actions</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
                <li><code>branch</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Commits summary info area</td>
        <td>org.bitbucket.commit.summary.info</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
                <li><code>commit</code></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Commits summary actions area</td>
        <td>org.bitbucket.commit.summary.actions</td>
        <td>
            <ul>
                <li><code>target_user</code></li>
                <li><code>user</code></li>
                <li><code>repository</code></li>
                <li><code>commit</code></li>
            </ul>
        </td>
    </tr>
</table>
