---
title: "Security overview"
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
- /jiracloud/security-overview.html
- /jiracloud/security-overview.md
date: "2017-09-11"
---
# Security overview

Implementing security is an essential part of integrating with Jira Cloud. It lets Atlassian applications protect customer data from unauthorized access and from malicious or accidental changes. It also allows administrators to install apps with confidence, letting users enjoy the benefits of apps in a secure manner.

There are two parts to securing your Jira app or integration: authentication and authorization. Authentication tells Jira Cloud the identity of your app, authorization determines what actions it can take within Jira.

## Authentication

Authentication is the process of identifying your app or integration to the Atlassian application and is the basis for all other security. Jira Cloud offers several authentication patterns, depending on whether you are building an Atlassian Connect app or calling the Jira REST APIs directly.

### Authentication for apps

Atlassian Connect uses JWT (JSON Web Tokens) to authenticate apps. This is built into the supported Atlassian Connect libraries.

When an app is installed, a security context is exchanged with the application. This context is used to create and validate JWT tokens, embedded in API calls. The use of JWT tokens guarantees that:

*   Jira Cloud can verify it is talking to the app, and vice versa (authenticity).
*   None of the query parameters of the HTTP request, nor the path (excluding the context path), nor the HTTP method, were altered in transit (integrity).

To learn more, read [understanding JWT for apps](../understanding-jwt/).


### Authentication for REST API requests

The Jira platform, Jira Software, and Jira Service Desk REST APIs can use one of the following three methods to authenticate clients directly. 

|  |  |
|------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| OAuth (1.0a) authentication | OAuth is a token-based authentication method, that uses request tokens generated from Jira Cloud to authenticate the client. We recommend that you use OAuth in most cases. It takes more effort to implement, but it is more flexible and secure compared to the other two authentication methods. <br>Read the [OAuth for REST APIs]. |
| Basic authentication | Basic authentication uses a predefined set of user credentials to authenticate the client. We recommend that you don't use basic authentication, except for tools like personal scripts or bots. It may be easier to implement, but it is much less secure. <br>Read the [Basic auth for REST APIs]. |
| Cookie-based authentication | Cookie-based authentication uses basic authentication to obtain a session cookie, then uses the cookie for additional requests. We recommend that you do not use cookie-based authentication in most cases, as OAuth is generally better. <br>Read the [Cookie-based auth for REST APIs]. |

## Authorization

Authorization is the process of allowing your app or integration to take certain actions in the Atlassian application, after it has been authenticated. Jira Cloud has different types of authorization, depending on whether you are building an Atlassian Connect app or calling the Jira REST APIs directly.

### Authorization for apps

Atlassian Connect provides two types of authorization: 

*   **Authorization via scopes and app users** — This is the default authorization method. You should use this for most cases.
*   **Authorization via JWT bearer token authorization grant type for OAuth 2.0** — You should only use this method when your app needs to make server-to-server requests on behalf of a user.

#### Authorization for apps via scopes and app users

This method has two levels of authorization: static authorization via scopes and run-time authorization via app users. 

| | |
|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Scopes       | Scopes are defined in the app descriptor and statically specify the maximum set of actions that an app may perform: read, write, etc. This security level is enforced by Atlassian Connect and cannot be bypassed by app implementations. To learn more, read our page on [scopes]. |
| App users | Every app is assigned its own user in a Cloud instance. In general, server-to-server requests are made by the app user. In some situations, the configuration of permission or issue security schemes in Jira Cloud by an administrator can cause an app user not to have permission to make a request. In Jira Cloud, Atlassian Connect will automatically attempt to resolve these errors each time an app is updated. <br> Client-side requests are made as the current user in the browser session, and are supported via the [`AP.request()` method](../jsapi/request/), or the app needs to make a server-to-server request using an OAuth 2.0 bearer token (see next section). |

<div class="aui-message tip">
    <div class="icon"></div>
    <p class="title">
        <strong>Combining static and run-time authorization</strong>
    </p>
    <p>
    The set of actions that an app is capable of performing is the intersection of the static scopes and the permissions of the user assigned to the request. This means that requests can be rejected because the assigned user lacks the required permissions. Therefore, your app should always defensively detect HTTP 403 forbidden responses from the product.
    </p>
</div>

#### Authorization for apps via JWT bearer token authorization grant type for OAuth 2.0

At a high level, this method works by the app exchanging a JWT for an OAuth 2.0 access token (provided by the application). The access token can be used to make server-to-server calls, on behalf of the user, to the application's API.

To learn more, read our page on [OAuth 2.0 - JWT Bearer token authorization grant type].  

### Authorization for REST API requests

Authorization for direct calls to the Jira Cloud REST APIs is based on the user used in the authentication process:

|  |  |
|------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| OAuth | The user who authorizes the request token, in the authentication process for the client, is used to make requests to Jira Cloud. |
| Basic authentication and Cookie-based authentication | The actions your client can perform are based on the permissions of the user credentials you are using. For example, if you are using basic authentication, your user must have Edit Issue permission on an issue in order to make a PUT request to the `/issue` resource. |

[authentication for apps]: /cloud/jira/platform/authentication-for-apps/
[OAuth for REST APIs]: /cloud/jira/platform/jira-rest-api-oauth-authentication
[Basic auth for REST APIs]: /cloud/jira/platform/jira-rest-api-basic-authentication
[Cookie-based auth for REST APIs]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication
[scopes]: ../scopes/
[architecture overview]: /cloud/jira/platform/architecture-overview
[`AP.request()` method]: /cloud/jira/platform/request
[OAuth 2.0 - JWT Bearer token authorization grant type]: /cloud/jira/platform/oauth-2-jwt-bearer-token-authorization-grant-type