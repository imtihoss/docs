-   **Building an app or integration with Jira using the [Atlassian Connect framework](../authentication-for-apps/)?** 
Atlassian Connect is the best solution, if you are developing for Jira Cloud. However, this page does not apply to Atlassian 
Connect. Authentication for Atlassian Connect apps is done using JSON Web Tokens (JWT), and is built into the Atlassian Connect 
libraries. For details, see [Authentication for apps](../authentication-for-apps/).
-   **Considering [OAuth](../jira-rest-api-oauth-authentication/)?** We recommend that you use OAuth over basic authentication, 
unless you are building tools like personal scripts or bots. With basic authentication, the username and password are sent 
repeatedly with requests and cached on the web browser, which is much less secure than OAuth (even if credentials are sent via 
	SSL/TLS for basic HTTP). OAuth requires more work to implement, but it uses a token-based workflow which is much more secure.
-   **[Cookie-based authentication](../jira-rest-api-cookie-based-authentication/)** is [deprecated and support will be removed 
in the future](/cloud/jira/platform/deprecation-notice-basic-auth-and-cookie-based-auth/). The replacement is [basic 
authentication with API tokens](/cloud/jira/platform/jira-rest-api-basic-authentication/). See the [deprecation notice](/cloud/jira/platform/deprecation-notice-basic-auth-and-cookie-based-auth/) for more information.